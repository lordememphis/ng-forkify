import { Component, OnInit } from '@angular/core';
import { Recipe, RecipesApiService } from '../data';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styles: [],
})
export class RecipesComponent implements OnInit {
  meals: Recipe[] = [];
  mealsCollection: Recipe[] = [];

  constructor(private readonly api: RecipesApiService) {}

  ngOnInit(): void {
    this.meals = this.api.getRecipes();
  }

  onAddMeal(meal: Recipe): void {
    this.mealsCollection.push(meal);
  }
}
