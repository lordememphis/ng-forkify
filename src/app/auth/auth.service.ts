import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../data';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private readonly router: Router) {}

  createAccount(data: any): boolean {
    let users: User[] = [];
    const uuid = `${data.username}${data.password}`;

    if (localStorage.getItem('users')) {
      users = JSON.parse(localStorage.getItem('users')!);
      const user = users.find((user) => user.username === data.username);
      if (user) {
        alert('User already exists');
        return false;
      }
    }
    users.push({ uuid, username: data.username, password: data.password });
    localStorage.setItem('users', JSON.stringify(users));
    return true;
  }

  login(data: any) {
    const uuid = `${data.username}${data.password}`;

    if (localStorage.getItem('users')) {
      const users: User[] = JSON.parse(localStorage.getItem('users')!);
      const user = users.find((user) => user.uuid === uuid);
      if (user) {
        localStorage.setItem('auth', 'true');
        localStorage.setItem('uuid', uuid);
        this.router.navigate(['/']);
        return;
      }
      alert('Account not found');
    } else {
      alert('Account not found');
    }
  }
}
