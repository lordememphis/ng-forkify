export interface Recipe {
  _id: string;
  approved_at: number;
  aspect_ratio: string;
  beauty_url: string;
  brand: RecipeBrand;
  brand_id: number;
  buzz_id: number;
  canonical_id: string;
  compilations: RecipeCompilation[];
  cook_time_minutes: number;
  country: string;
  created_at: number;
  credits: RecipeCredit[];
  description: string;
  draft_status: string;
  facebook_posts: [];
  id: number;
  inspired_by_url: string;
  instructions: RecipeInstruction[];
  is_one_top: boolean;
  is_shoppable: boolean;
  keywords: string;
  language: string;
  name: string;
  num_servings: number;
  nutrition: RecipeNutrition;
  nutrition_visibility: string;
  original_video_url: string;
  prep_time_minutes: number;
  promotion: string;
  renditions: RecipeRendition[];
  sections: RecipeSection[];
  seo_title: string;
  servings_noun_plural: string;
  servings_noun_singular: string;
  show: { id: number; name: string };
  show_id: number;
  slug: string;
  tags: RecipeTag[];
  thumbnail_alt_text: string;
  thumbnail_url: string;
  tips_and_ratings_enabled: boolean;
  topics: { name: string; slug: string }[];
  total_time_minutes: number;
  total_time_tier: { display_tier: string; tier: string };
  updated_at: number;
  user_ratings: {
    count_negative: number;
    count_positive: number;
    score: number;
  };
  video_ad_content: string;
  video_id: number;
  video_url: string;
  yields: string;
}

export interface RecipeBrand {
  id: number;
  image_url: string;
  name: string;
  slug: string;
}

export interface RecipeCompilation {
  approved_at: number;
  aspect_ratio: string;
  beauty_url: string;
  buzz_id: string;
  canonical_id: string;
  country: string;
  created_at: number;
  description: string;
  draft_status: string;
  facebook_posts: unknown[];
  id: number;
  is_shoppable: boolean;
  keywords: string[];
  language: string;
  name: string;
  promotion: string;
  show: { id: number; name: string }[];
  slug: string;
  thumbnail_alt_text: string;
  thumbnail_url: string;
  video_id: number;
  video_url: string;
}

export interface RecipeCredit {
  id: number;
  image_url: string;
  name: string;
  slug: string;
  type: string;
}

export interface RecipeInstruction {
  appliance: string;
  display_text: string;
  end_time: number;
  id: number;
  position: number;
  start_time: number;
  temperature: number;
}

export interface RecipeNutrition {
  calories: number;
  carbohydrates: number;
  fat: number;
  fiber: number;
  protein: number;
  sugar: number;
  updated_at: string;
}

export interface RecipeRendition {
  aspect: string;
  bit_rate: number;
  container: string;
  content_type: string;
  duration: number;
  file_size: number;
  height: number;
  maximum_bit_rate: number;
  minimum_bit_rate: number;
  name: string;
  poster_url: string;
  url: string;
  width: number;
}

export interface RecipeSection {
  components: RecipeSectionComponent[];
  name: string;
  position: number;
}

interface RecipeSectionComponent {
  extra_comment: string;
  id: number;
  ingredient: RecipeSectionComponentIngredient;
  measurements: RecipeSectionComponentMeasurement[];
  position: number;
  raw_text: string;
}

interface RecipeSectionComponentIngredient {
  created_at: number;
  display_plural: string;
  display_singular: string;
  id: number;
  name: string;
  updated_at: number;
}

interface RecipeSectionComponentMeasurement {
  id: number;
  quantity: string;
  unit: {
    abbreviation: string;
    display_plural: string;
    display_singular: string;
    name: string;
    system: string;
  };
}

export interface RecipeTag {
  display_name: string;
  id: number;
  name: string;
  type: string;
}
