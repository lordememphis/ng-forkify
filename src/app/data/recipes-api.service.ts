import { Injectable } from '@angular/core';
import recipes from '../../assets/recipes.data.json';
import { Recipe } from './recipe.interfaces';

@Injectable({ providedIn: 'root' })
export class RecipesApiService {
  getRecipes(): Recipe[] {
    return recipes as unknown as Recipe[];
  }

  getRecipe(id: string): Recipe {
    return recipes.find((recipe) => recipe.id === +id) as unknown as Recipe;
  }
}
